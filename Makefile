ifndef TOOL
$(error TOOL is not set. Usage; TOOL=<your_tool> make <target>)
endif

PEP8_DOCKER_IMAGE=pipelinecomponents/flake8
PEP8_CONFIG_FILE=.flake8
IMAGE_BASE=registry.gitlab.com/acesrg/optimal-control/sandbox
SHELL = /bin/sh

CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

lint:
	docker run --rm -it -v $(PWD):/project -w /project ${PEP8_DOCKER_IMAGE} flake8 --config $(PEP8_CONFIG_FILE) .


pull:
	docker pull ${IMAGE_TAG}/${TOOL}	

build:
	docker build -f .gci/Dockerfile.${TOOL} -t ${IMAGE_BASE}/${TOOL} .

run:
	docker run -it \
		-v $(PWD):$(PWD) -w $(PWD) \
		-u $(CURRENT_UID):$(CURRENT_GID) \
		-v /etc/passwd:/etc/passwd:ro \
		${IMAGE_BASE}/${TOOL} \
		bash

.PHONY: lint pull build run
