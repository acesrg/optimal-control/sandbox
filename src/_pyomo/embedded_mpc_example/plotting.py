from itertools import chain

import numpy as np

from .control_plot import (
    ControlPlot,
    SignalPlot,
    SurfacePlot,
)


def plot_mpc_result(m):
    # solution data at sample times
    h = m.h()
    K = np.array([k for k in m.k])
    u = [m.u[k]() for k in K]
    y = [m.y[k]() for k in K]
    v = [m.x[2, k]() for k in K]

    # interpolate between sample times
    t = np.linspace(0, h)
    tp = [_ for _ in chain.from_iterable(k * h + t for k in K[:-1])]
    up = [_ for _ in chain.from_iterable(u[k] + t * 0 for k in K[:-1])]
    yp = [_ for _ in chain.from_iterable(
        y[k] + t * (v[k] + t * u[k] / 2) for k in K[:-1]
    )]
    vp = [_ for _ in chain.from_iterable(v[k] + t * u[k] for k in K[:-1])]

    # plot
    plot = ControlPlot(size=(10, 5))
    SignalPlot(
        parent=plot, pos=(3, 2, 1),
        title='position',
        plot=(tp, yp, 'r--', h * K, y, 'bo'),
        ylim=[-1.1, 1.1],
    )
    SignalPlot(
        parent=plot, pos=(3, 2, 3),
        title='velocity',
        plot=(tp, vp, 'r--', h * K, v, 'bo'),
        ylim=[-1.1, 1.1],
    )
    SignalPlot(
        parent=plot, pos=(3, 2, 5),
        title='control force  u[0] = {0:<6.3f}'.format(u[0]),
        plot=(
            np.append(tp, K[-1] * h),
            np.append(up, u[-1]),
            'r--', h * K, u, 'bo'
        ),
        ylim=[-1.1, 1.1],
    )
    SignalPlot(
        parent=plot, pos=(1, 2, 2),
        aspect=('equal', 'box'),
        title='phase plane', xlabel='position', ylabel='velocity',
        plot=(yp, vp, 'r--', y, v, 'bo'),
        xlim=[-1.1, 1.1], ylim=[-1.1, 1.1],
    )
    plot.save("design.png")


def plot_output_for_inputs(Y, V, U):
    plot = ControlPlot(size=(10, 8))
    plot = SurfacePlot(
        parent=plot, pos=(1, 1, 1),
        plot=(V, Y, U),
        xlabel='position', ylabel='velocity', zlabel='control force',
        xlim=[-0.7, 0.7], ylim=[-0.7, 0.7], zlim=[-1.1, 1.1],
    )
    plot.save("output_for_any_input.png")


def plot_online_mpc(t, x, u, noise):
    plot = ControlPlot(size=(10, 8))
    SignalPlot(
        parent=plot, pos=(3, 1, 1),
        plot=(t, noise),
        ylim=[-0.15, 0.15],
        xlabel='time', legend=['position noise', 'velocity noise'],
    )
    SignalPlot(
        parent=plot, pos=(3, 1, 2),
        plot=(t, x),
        ylim=[-1.1, 1.1],
        xlabel='time', legend=['position', 'velocity'],
    )
    SignalPlot(
        parent=plot, pos=(3, 1, 3),
        plot=(t, u),
        ylim=[-1.1, 1.1],
        xlabel='time', ylabel='control input',
    )
    plot.save('online_mpc.png')
