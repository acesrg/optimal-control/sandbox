import logging

import matplotlib.pyplot as plt


class ControlPlot():
    def __init__(self, fig=None, tight=True, size=None):
        if fig is None:
            self.fig = plt.figure(figsize=size)
        else:
            self.fig = fig
        self.__tight = tight
        self.log = logging.getLogger('control_plot')

    @staticmethod
    def __safe_pop(arg, kw, default):
        try:
            return arg.pop(kw)
        except KeyError:
            return default.pop(kw)

    def __set_common(self, **kwargs):
        defaults = {
            'title': None,
            'xlabel': None,
            'ylabel': None,
            'xlim': None,
            'ylim': None,
            'legend': (),
            'aspect': ('auto', 'box'),
        }
        self.ax.set_title(self.__safe_pop(kwargs, 'title', defaults))
        self.ax.set_xlabel(self.__safe_pop(kwargs, 'xlabel', defaults))
        self.ax.set_ylabel(self.__safe_pop(kwargs, 'ylabel', defaults))
        self.ax.set_xlim(self.__safe_pop(kwargs, 'xlim', defaults))
        self.ax.set_ylim(self.__safe_pop(kwargs, 'ylim', defaults))
        self.ax.legend(self.__safe_pop(kwargs, 'legend', defaults))

    def __set_style(self, **kwargs):
        defaults = {
            'grid': True,
            'aspect': ('auto', 'box'),
        }
        self.ax.grid(self.__safe_pop(kwargs, 'grid', defaults))
        self.ax.set_aspect(*self.__safe_pop(kwargs, 'aspect', defaults))

    def _set_configs(self, **kwargs):
        self.__set_common(**kwargs)
        self.__set_style(**kwargs)

    def __finishing(self):
        if self.__tight:
            self.fig.tight_layout()

    def show(self, *args, **kwargs):
        self.__finishing()
        self.fig.show(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.__finishing()
        self.fig.savefig(*args, **kwargs)
        self.log.info(f"plot saved as: {args[0]}")


class SignalPlot(ControlPlot):
    def __init__(
        self,
        parent=None,
        pos=(1, 1, 1),
        plot=None,
        **kwargs,
    ):
        super().__init__(parent.fig)
        self.ax = self.fig.add_subplot(*pos)

        if plot:
            self.ax.plot(*plot)
        self._set_configs(**kwargs)


class SurfacePlot(ControlPlot):
    def __init__(
        self,
        parent=None,
        pos=(1, 1, 1),
        plot=None,
        zlabel=None,
        zlim=None,
        **kwargs,
    ):
        super().__init__(parent.fig)
        self.ax = self.fig.add_subplot(*pos, projection='3d')

        if plot:
            self.ax.plot_surface(*plot)
        self.ax.set_zlabel(zlabel)
        self.ax.set_zlim(zlim)
        self._set_configs(**kwargs)
