## Embedded MPC example using pyomo

### Use

to iterate over the controller design, with defined initial conditions:
```bash
python3 -m src._pyomo.embedded_mpc_example.design
```

it outputs:

![image](/uploads/d66dae8c7c3188c0b7433bdb3931f667/image.png)

---

to calculate every output for any input combination, and save the data for further online runs:

```bash
python3 -m src._pyomo.embedded_mpc_example.main --precalculate
```

besides the raw data, also outputs:

![image](/uploads/e80fd4fca40444db16fcaaad30c875ee/image.png)

---

finally, to run an online closed loop controller using said data as a lookup table:

```bash
python3 -m src._pyomo.embedded_mpc_example.main --simulate
```

outputs:

![image](/uploads/e25ed0abc2bf876cc78fb64a88e56efc/image.png)

### source

this is a refactor from: [jckantor pyomo cookbook](https://github.com/jckantor/ND-Pyomo-Cookbook/blob/master/notebooks/02.06-Model-Predictive-Control-of-a-Double-Integrator.ipynb)
