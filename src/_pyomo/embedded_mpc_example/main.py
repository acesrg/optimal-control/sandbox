import argparse
import pickle

import numpy as np
from scipy.integrate import odeint

from .plotting import plot_online_mpc
from .design import mpc_double_integrator
from .precalculate import precalculate


def precalculate_model():
    model = mpc_double_integrator(10)
    model.h = 0.5
    model.gamma = 0.4

    output_table = precalculate(model)

    with open('data.pickle', 'wb') as f:
        pickle.dump(output_table, f, pickle.HIGHEST_PROTOCOL)


def generate_noise_array(length):
    noise = np.zeros((length, 2))
    noise_pos_magnitude = 0.05
    noise[90:105, 0] = noise_pos_magnitude

    noise_vel_magnitude = 0.02
    noise[80:103, 1] = noise_vel_magnitude
    return noise


def simulate():
    with open('data.pickle', 'rb') as f:
        output_table = pickle.load(f)

    def get_outputs(x, t):
        y, v = x
        u = output_table(y, v)
        return v, u

    t = np.linspace(0, 10, 200)
    n = generate_noise_array(200)

    # set initial conditions
    x0 = [-0.75, -0.75]

    # store x
    x = []
    for i, _ in enumerate(t):
        _x = odeint(get_outputs, x0, t[i:])
        x.append(_x[0])
        if i == len(t) - 1:
            break

        x0 = [_x[1][0] + n[i][0], _x[1][1] + n[i][1]]

    u = [output_table(x1, x2) for x1, x2 in x]

    plot_online_mpc(t, x, u, n)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='MPC pyomo example.')
    parser.add_argument('--precalculate', action='store_true',
                        help='calculate MPC for every input and save')
    parser.add_argument('--simulate', action='store_true',
                        help='use calculated output to simulate live MPC')
    args = parser.parse_args()

    if args.precalculate:
        print('precalculating outputs ...')
        precalculate_model()
        print('done')

    if args.simulate:
        print('running simulation ...')
        simulate()
        print('done')
