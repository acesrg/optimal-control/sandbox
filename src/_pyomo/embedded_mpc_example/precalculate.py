import logging

import numpy as np
from scipy.interpolate import interp2d

import pyomo.environ as pyo

from .plotting import plot_output_for_inputs


# turn off pyomo warnings
logging.getLogger('pyomo.core').setLevel(logging.ERROR)


def precalculate(model, plot=True):
    """Gives every optimal output for any given inputs
    """

    def get_every_out(y, v):
        u = 0 * y
        for i in range(0, len(y)):
            model.ic[1] = y[i]
            model.ic[2] = v[i]
            results = pyo.SolverFactory('cbc').solve(model)
            if str(results.solver.termination_condition) == 'optimal':
                u[i] = model.u[0]()
            else:
                u[i] = None
        return u

    y = v = np.arange(-0.7, 0.7, 0.1)
    Y, V = np.meshgrid(y, v)
    u = np.array(get_every_out(np.ravel(Y), np.ravel(V)))
    U = u.reshape(V.shape)

    if plot:
        plot_output_for_inputs(Y, V, U)

    return interp2d(Y, V, U)
