import pyomo.environ as pyo

from .plotting import plot_mpc_result


class Constraints:
    """Stores constraints, they should take args (m, k) and return boolean
    Static class, does not intend to create objects
    """
    N = None

    @staticmethod
    def initial_cond(m, k):
        return m.x[k, 0] == m.ic[k]

    @staticmethod
    def u_sum(m, k):
        return m.u[k] == m.upos[k] - m.uneg[k]

    @staticmethod
    def y_sum(m, k):
        return m.y[k] == m.ypos[k] - m.yneg[k]

    @staticmethod
    def x1_model(m, k):
        """only if k < N"""
        if k < Constraints.N:
            return m.x[1, k + 1] == m.x[1, k] + m.h * m.x[2, k] + \
                m.h ** 2 * m.u[k] / 2
        else:
            return pyo.Constraint.Skip

    @staticmethod
    def x2_model(m, k):
        """only if k < N"""
        if k < Constraints.N:
            return m.x[2, k + 1] == m.x[2, k] + m.h * m.u[k]
        else:
            return pyo.Constraint.Skip

    @staticmethod
    def y_model(m, k):
        return m.y[k] == m.x[1, k]


def mpc_double_integrator(N=2, h=1):
    Constraints.N = N
    m = pyo.ConcreteModel()
    m.states = pyo.RangeSet(1, 2)
    m.k = pyo.RangeSet(0, N)

    m.h = pyo.Param(initialize=h, mutable=True)
    m.ic = pyo.Param(m.states, initialize={1: 0.5, 2: 0.5}, mutable=True)
    m.gamma = pyo.Param(default=0.5, mutable=True)

    m.x = pyo.Var(m.states, m.k)
    m.icfix = pyo.Constraint(m.states, rule=Constraints.initial_cond)
    m.x[1, N].fix(0)
    m.x[2, N].fix(0)

    m.u = pyo.Var(m.k, bounds=(-1, 1))
    m.upos = pyo.Var(m.k, bounds=(0, 1))
    m.uneg = pyo.Var(m.k, bounds=(0, 1))
    m.constraint_usum = pyo.Constraint(m.k, rule=Constraints.u_sum)

    m.y = pyo.Var(m.k, bounds=(-1, 1))
    m.ypos = pyo.Var(m.k, bounds=(0, 1))
    m.yneg = pyo.Var(m.k, bounds=(0, 1))
    m.constraint_ysum = pyo.Constraint(m.k, rule=Constraints.y_sum)

    m.constraint_x1 = pyo.Constraint(m.k, rule=Constraints.x1_model)
    m.constraint_x2 = pyo.Constraint(m.k, rule=Constraints.x2_model)
    m.constraint_y = pyo.Constraint(m.k, rule=Constraints.y_model)

    m.uobj = m.gamma * sum(m.upos[k] + m.uneg[k] for k in m.k)
    m.yobj = (1 - m.gamma) * sum(m.ypos[k] + m.yneg[k] for k in m.k)
    m.obj = pyo.Objective(expr=m.uobj + m.yobj, sense=pyo.minimize)

    return m


def mpc_design():
    model = mpc_double_integrator(5, 0.5)
    model.ic[1] = 0.5
    model.ic[2] = 0.5
    model.gamma = 0

    results = pyo.SolverFactory('cbc').solve(model)
    if str(results.solver.termination_condition) != "optimal":
        print(results.solver.termination_condition)
        raise RuntimeError()

    plot_mpc_result(model)


if __name__ == "__main__":
    mpc_design()
