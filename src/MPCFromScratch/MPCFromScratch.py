#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: ibrahim
Source: Model Predictive Control System Design and Implementation Using MATLAB
"""

import numpy as np
from scipy.linalg import block_diag, expm
from scipy.integrate import RK45
import matplotlib.pyplot as plt
import timeit

class MPC:
    def __init__(self, A, B, C, Np=10, Nc=4, uMax=1, uMin=0, duMax=1, duNMax=1, yMax=5, T=0.001, discretize=True, **kwargs):
        """
        Class used to implement a basic MPC Controller.

        Arguments:
            A: State-space A matrix.
            B: State-space B matrix.
            C: State-space C matrix.
            Np: Prediction horizon.
            Nc: Control horizon.
            uMax: Maximum input constraint.
            uMin: Minimum input constraint.
            duMax: Maximun input incremental variation constraint.
            duNMax: Maximun input negative incremental variation constraint.
            T: Discretization sample time.
            discretize: Discretization enable or disable.
        """
        self.A = A
        self.B = B
        self.C = C
        self.x = np.zeros((self.A.shape[0],2), dtype=float)
        self.u = np.zeros((self.B.shape[1],1), dtype=float)
        self.y = np.zeros((self.C.shape[0],1), dtype=float)
        self.T = T
        if discretize:
            self.A = expm(self.A*self.T)
            self.B = np.linalg.inv(A).dot((self.A - np.eye(self.A.shape[0]))).dot(self.B)
        self.Aa = np.r_[np.c_[self.A, np.zeros((self.A.shape[0],self.C.shape[0]))], np.c_[self.C.dot(self.A), np.eye(self.C.shape[0])]]
        self.Ba = np.r_[self.B, self.C.dot(self.B)]
        self.Ca = np.c_[self.C, np.eye(self.C.shape[0])]
        self.Np = Np
        self.Nc = Nc
        self.F = self.getF()
        self.P = self.getP()
        self.r = np.zeros((self.y.shape[0],1))
        self.Rw = np.zeros((self.y.shape[0],1))
        self.H = self.getH()
        self.M = self.getM()
        self.uMax = np.array([[uMax]])
        self.uMin = np.array([[uMin]])
        self.duMax = np.array([[duMax]])
        self.duNMax = np.array([[duNMax]])
        self.yMax = self.getyMax(yMax)
    
    def getyMax(self, yMax):
        self.yMax = yMax
        for i in range(1,Np):
            self.yMax = np.vstack((self.yMax, yMax))
        return self.yMax

    def getM(self):# Eq. (2.25)
        self.M = np.tril(np.ones((self.Nc,self.Nc), dtype=float))
        self.M = np.vstack((np.vstack((np.vstack((np.vstack((self.M, -self.M)),
                            np.eye(self.Nc))), 
                            -np.eye(self.Nc))),
                            self.P))

    def getF(self): # Eq. (1.44)
        F = self.Ca.dot(self.Aa)
        for i in range(1,self.Np):
            F = np.vstack((F, self.Ca.dot(np.linalg.matrix_power(self.Aa,i+1))))
        return F
                
    def getP(self): # Eq. (1.44)
        P = np.zeros((self.Ca.shape[0], self.Nc * self.Ca.shape[0]))
        P[:,0:self.Ca.shape[0]] = self.Ca.dot(self.Ba)
        for i in range(1,self.Np):
            row = np.roll(P[-self.Ca.shape[0]:,:],1)
            row[:,0:self.Ca.shape[0]] = (self.Ca.dot(np.linalg.matrix_power(self.Aa,i))).dot(self.Ba)
            P = np.r_[P, row]
        return P

    def getH(self):
        self.R = self.Rw[0]*np.eye(self.Nc)
        for i in range(1,self.y.shape[0]):
            self.R = block_diag(self.R, self.Rw[i]*np.eye(self.Nc))
        H = self.P.T.dot(self.P) + self.R
        return H
    
    def setReference(self, ref):
        self.ref = np.array([[ref]])
        for i in range(1, self.Np):
            self.ref =  np.vstack((self.ref, ref))

    def setRw(self, rw):
        for i in range(rw.shape[0]):
            self.Rw[i] = rw[i]   
        self.H = self.getH() 

    def modelOptimization(self, x_actual):
        deltaU = np.dot(self.H, (np.transpose(self.P)@((self.ref) - np.dot(self.F, x_actual.T)))) # Eq. (1.16)
        return deltaU

    def getEqpFqp(self, actualU, x_actual):
        Eqp = 2 * (np.dot(np.transpose(self.P), self.P) + self.R) # Eq. (2.27) based on (1.14)
        Fqp = - 2 * np.dot(np.transpose(self.P), (self.ref - np.dot(self.F, x_actual.T))) # Eq. (2.27) based on (1.14)
        actualU = np.array([[actualU]])
        gamma = self.uMax - actualU
        for i in range(1,self.Nc):
            gamma = np.vstack((gamma, (self.uMax - actualU)))
        for i in range(0,self.Nc):
            gamma = np.vstack((gamma, (self.uMin + actualU)))
        for i in range(0,self.Nc):
            gamma = np.vstack((gamma, (self.duMax)))
        for i in range(0,self.Nc):
            gamma = np.vstack((gamma, (self.duNMax)))
        gamma = np.vstack((gamma, (self.yMax - self.F @ x_actual.T)))
        return Eqp, Fqp, gamma
    
    def QPhild(self, Vin, x_actual):
        """
        Function used to implement Hildreth’s quadratic programming procedure.
        """
        noOptimal = False
        E, F, gamma = self.getEqpFqp(Vin, x_actual)
        self.getM()
        M = self.M 
        eta = - np.dot(np.linalg.inv(E),F)
        kk = 0
        for i in range(0, M.shape[0]):
            x = M[i,:]@eta # Eq. (2.34)
            if((x) > gamma[i]):
                kk = kk + 1
            else:
                kk = kk + 0
        if(kk == 0):
            return eta
        H = np.dot(M, np.dot(np.linalg.inv(E),np.transpose(M)))# Eq. (2.33)
        K = np.dot(M, np.dot(np.linalg.inv(E),F)) + gamma# Eq. (2.33)
        [n, m] = K.shape
        x_ini = np.zeros((n,m))
        lambd = x_ini
        al = 10
        for km in range(1,39):
            for i in range(1,n+1):
                w = np.dot(H[i-1,:], lambd) - np.dot(H[(i-1),(i-1)], lambd[(i-1),0])# Eq. (2.61)
                w = w + K[(i-1),0]# Eq. (2.61) -> (w + ki)
                la= -w/H[(i-1),(i-1)]# Eq. (2.61) -> (-w/hii)
                lambd[(i-1),0] = max(0,la)
            if km == 1:
                lambda_p = np.zeros((n,m))
            al = np.dot(np.transpose(lambd-lambda_p),(lambd - lambda_p))
            lambda_p = np.array(lambd)
            if(al < 1e-8):
                break
            if(km == 38):
                print("No optimal solution")
                noOptimal = True
                break
        if(noOptimal == True):
            eta = "No optimal"#np.zeros(self.Nc)
        else:    
            eta = - np.dot(np.linalg.inv(E),F) - np.dot(np.dot(np.linalg.inv(E),np.transpose(M)), lambd)# Eq. (2.34)
        return eta

class DCMotor():
    """
    Class used to mimic DC Motor behavior.
    Source: https://ctms.engin.umich.edu/CTMS/index.php?example=MotorSpeed&section=SystemModeling

    Parameters:
        b: Motor viscous friction constant [N.m.s]
        J: Moment of inertia of the rotor [kg.m^2]
        K: Includes: Electromotive force constant (Ke [V/rad/sec]) and  Motor torque constant (Kt [N.m/Amp])
        R: Electric resistance [Ohm]
        L: Electric inductance [H]

    """
    def __init__(self, b = 0.01, J = 0.00001, K = 0.01, R = 2, L = 0.05):
        self.b = b
        self.J = J
        self.K = K
        self.R = R
        self.L = L
    def stateSpaceMatrices(self):
        self.A = np.array([[-self.b/self.J,   self.K/self.J], [-self.K/self.L,  -self.R/self.L]])
        self.B = np.array([[0], [1/self.L]])
        self.C = np.array([[1, 0]])
        return self.A, self.B, self.C

def generate_noise_array(length):
    noise = np.zeros((length, 2))
    noise_pos_magnitude = 0.9
    noise[90:300, 0] = noise_pos_magnitude

    noise_vel_magnitude = 0.015
    noise[80:103, 1] = noise_vel_magnitude
    return noise

if __name__ == '__main__':
    realMotor = DCMotor(b = 0.01, J = 0.00005, K = 0.01, R = 2, L = 0.05)
    A, B, C = realMotor.stateSpaceMatrices()
    motorMPC = DCMotor()
    AMPC, BMPC, CMPC = motorMPC.stateSpaceMatrices()
    Vin = 0
    Np = 4
    Nc = 1
    vMax = 6
    vMin = 0
    dvMax = 5#0.05
    dvNMax = 5#0.02
    yMax = np.array([[1.04]])
    mpc = MPC(AMPC, BMPC, CMPC, Np, Nc, vMax, vMin, dvMax, dvNMax, yMax)
    mpc.setRw(np.array([0.1]))
    mpc.setReference(1)
    i_pass = 0
    i_actual = 0
    w_pass = 0
    w_actual = 0
    v = []
    w = []
    X0 = np.array([[0.1],[0.1]])
    Vin = 0
    x_actual = np.array([[0, 0, 0]])
    time_step = 0.01
    t_upper = 0.01
    t_lower = 0
    initial_conditions = [0, 0]  # [Angular Speed[rad/s], Current[A]]
    tValues = []
    wValues = []
    iValues = []
    vValues = []
    passState = np.zeros((2,1))
    actualState = np.zeros((2,1))
    states = []
    samples = 500
    MPCtime = 0
    plotyMax = np.ones(samples) * yMax
    plotvMax = np.ones(samples) * vMax
    n = generate_noise_array(samples)
    for i in range(0, samples):
        timeCountStart = timeit.default_timer()
        optimal_dU = mpc.QPhild(Vin, x_actual)
        timeCountEnd = timeit.default_timer()
        MPCtime += (timeCountEnd - timeCountStart)
        if(optimal_dU == "No optimal"):
            Vin = 0
        else:
            Vin += float(optimal_dU[0])
        vValues.append(Vin)
        solution = RK45(fun=lambda t,X: np.dot(A, X) + np.dot(B, Vin), t0=t_lower, y0=initial_conditions, t_bound=(t_lower + time_step), vectorized=True, rtol = 1e-5)
        solution.step()
        tValues.append(solution.t)
        wValues.append(solution.y[0] + n[i][0])
        iValues.append(solution.y[1]+ n[i][0] * 0)
        t_lower = solution.t
        initial_conditions = solution.y
        w_actual = solution.y[0] + n[i][0]
        i_actual = solution.y[1] + n[i][0] * 0
        x_actual = np.array([np.hstack((w_actual - w_pass, i_actual - i_pass, w_actual))])
        w_pass = w_actual
        i_pass = i_actual
    print("MPC average iteration time: " + str(MPCtime/samples))
    plt.plot(tValues, plotyMax[0,:], 'y-.',label="yMax")
    plt.plot(tValues, plotvMax, 'k-.',label="vMax")
    plt.plot(tValues, n[:,0], 'k',label="n")
    plt.plot(tValues, iValues, 'r',label="Current [A]")
    plt.plot(tValues, vValues, 'b',label="Voltaje [V]")
    plt.plot(tValues, wValues, 'g',label="Angular Speed [rad/s]")
    plt.xlabel("Time")
    plt.title("DC-Motor with MPC controller")
    plt.legend()
    plt.grid()
    #plt.savefig('MPC-OutputConstraint.png')
    plt.show()
