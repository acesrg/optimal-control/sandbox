## Optimal control sandbox

A sandbox to play with solvers, problems, etc. The idea is not having to install anything in your computer.
Just use the containers generated here, or build your own and push them.

### Usage

first time:
```
TOOL=<tool> make pull
```

```
TOOL=<tool> make run
```
